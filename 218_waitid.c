/* 
   Copyright (C) 2004-2012 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <http://www.gnu.org/licenses/>.  */

#include <shlib-compat.h>
#include <stddef.h>
#include <errno.h>
#include <sys/wait.h>
#include <sysdep.h>

/* for now only the wrapper implementation */
/* later on we will try to use wait6 when available */

#define waitid __unused_waitid_alias
#include <sysdeps/posix/waitid.c>
#undef waitid

versioned_symbol (libc, __waitid, waitid, GLIBC_2_18);

#if SHLIB_COMPAT (libc, GLIBC_2_1, GLIBC_2_18)

/* it used to be: */

#define OLD_P_ALL	0
#define OLD_P_PID	1
#define OLD_P_PGID	2

int
__waitid_old (idtype_t oldtype, id_t id, siginfo_t *infop, int options)
{
  idtype_t newtype;

  switch (oldtype)
  {
      case OLD_P_ALL:
          newtype = P_ALL;
      break;
      case OLD_P_PID:
          newtype = P_PID;
      break;
      case OLD_P_PGID:
          newtype = P_PGID;
      break;
      default:
          newtype = oldtype;
   }
  return __waitid(newtype, id, infop, options);
}
compat_symbol (libc, __waitid_old, waitid, GLIBC_2_1);
#endif
