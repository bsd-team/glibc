#include <pthread.h>
#include <sys/syscall.h>
#include <stdio.h>
  
static inline long __thr_self()
{ 
  long ktid;
  syscall(SYS_thr_self, &ktid);
  return  ktid;
};

static inline int sys_getpid()
{
  return syscall(SYS_getpid);
}

void *doit(void*p)
{
int i;
i = getpid();
printf("%s: doit a %d %d %ld\n", p, i, sys_getpid(), __thr_self());
sleep(2);
i = getpid();
printf("%s: doit b %d %d %ld\n", p, i, sys_getpid(),__thr_self());
}

int main()
{
int i;
pthread_t pt;
doit("begin");
printf("going to fork a\n");
i = fork();
printf("ahoj c %d %d %ld\n", i, sys_getpid(),__thr_self());
i = pthread_create(&pt, NULL, doit, "thread");
printf("ahoj create %d %d %ld\n", i, sys_getpid(),__thr_self());
sleep(1);
printf("going to fork b\n");
i = fork();
printf("ahoj d %d %d %ld\n", i, sys_getpid(),__thr_self());
doit("end");
return 5 ;
}