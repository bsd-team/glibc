/*
 
 Under Linux kernel uid/gid are per thread, library have to sync them.
 Under FreeBSD kernel uid/gid are per process.
 
 */
 
 

#include <pthread.h>
#include <unistd.h>
#include <sys/syscall.h>   /* For SYS_xxx definitions */
#include <stdio.h>

              
void * doit(void *v)
{
   int rv;
   sleep(1);
   rv = syscall(SYS_getgid,0);
   printf("get1 :%d %d\n", rv, getgid());
   rv = syscall(SYS_setgid, 1000);
   printf("set1 :%d\n", rv);
   rv = syscall(SYS_getgid,0);
   printf("get2 :%d %d\n", rv, getgid());
   sleep(2);
   setgid(2000);
   rv = syscall(SYS_getgid,0);
   printf("get after standard set :%d %d\n", rv, getgid());
   
};



int main()
{
   int rv;
   pthread_t th;
   rv = syscall(SYS_getgid,0);
   printf("get main 1:%d %d\n", rv, getgid());
   pthread_create(&th, NULL, doit, NULL);
   rv = syscall(SYS_getgid,0);
   printf("get main 2:%d %d\n", rv, getgid());
   sleep(2);
   rv = syscall(SYS_getgid,0);
   printf("get main 3:%d %d\n", rv, getgid());
   sleep(2);
   rv = syscall(SYS_getgid,0);
   printf("get main 4:%d %d\n", rv, getgid());
   sleep(1);
   printf("main near to exit\n");
   sleep(1);
}
