#!/bin/sh
set -ex
LANG=C

# fbtl and fbtl_db dirs
tmp=`mktemp -d`
mkdir -p ${tmp}/b/
cp -a fbtl ${tmp}/b/
cp -a fbtl_db ${tmp}/b/
(cd ${tmp} && diff -x .svn -Nurd null b/ ) | \
   sed -e "sz^--- null/fbtl.*z--- /dev/nullz" \
       -e "\z^diff -x .svn -Nurd null/fbtl.*zd" \
       -e "\z^+++ b/fbtlzsz\t20[0-9][0-9].*zz" \
   > fbtl.diff
#rm -rf ${tmp}
